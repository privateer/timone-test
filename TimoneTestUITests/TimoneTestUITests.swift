//
//  TimoneTestUITests.swift
//  TimoneTestUITests
//
//  Created by Jindrich Skeldal on 15/11/2019.
//  Copyright © 2019 Skeldal. All rights reserved.
//

import XCTest

class TimoneTestUITests: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCreateAndCancelNewNote() {
        
        //let app = XCUIApplication()
        app.navigationBars["TimoneTest.NotesListView"].buttons["Add"].tap()
        app.navigationBars["TimoneTest.NoteEditView"].buttons["Back"].tap()

        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

}
