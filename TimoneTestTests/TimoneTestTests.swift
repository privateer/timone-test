//
//  TimoneTestTests.swift
//  TimoneTestTests
//
//  Created by Jindrich Skeldal on 14/11/2019.
//  Copyright © 2019 Skeldal. All rights reserved.
//

import XCTest
@testable import TimoneTest

class TimoneTestTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testServerDownloadList() {
        let promise = expectation(description: "Wait for response")
        
        NotesServer.shared.downloadNotesList { success in
            if success {
                promise.fulfill()
            } else {
                XCTFail("Note list failed")
            }
        }
        
        wait(for: [promise], timeout: 5)
    }
    
    func testServerAddNoteAny() {
        let promise = expectation(description: "Wait for response")
        
        NotesServer.shared.createNote(title: "New note") { success, note in
            XCTAssert(success, "Note creation failed")
            XCTAssert(note != nil, "Note creation succeeded but is empty")
            promise.fulfill()
        }
        
        wait(for: [promise], timeout: 5)
    }
    
    func testServerAddNoteSameTitle() {
        let promise = expectation(description: "Wait for response")
        
        let title = "New note"
        NotesServer.shared.createNote(title: title) { success, note in
            XCTAssert(success, "Note creation failed")
            XCTAssert(note != nil, "Note creation succeeded but is empty")
            XCTAssert(note?.title==title, "Note creation succeeded but return wrong title")
            promise.fulfill()
        }
        
        wait(for: [promise], timeout: 5)
    }
}
