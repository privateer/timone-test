Jindřich Skeldal, 2019
jindrich@skeldal.com

Notes
-----
The app work just with data stored on server.

I am not sure if I miss something but server returns just example values no matter what I send. That is why you can see just two notes in the list and create nor modify any note.

GUI
---
To confirm creation or note update I display what server returns before I roll back to list.

Tests
-----
testServerAddNoteSameTitle() utilize the fact data on server are static so it always fail.