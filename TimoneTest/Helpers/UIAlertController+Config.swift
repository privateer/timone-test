//
//  UIAlertController+Config.swift
//  TimoneTest
//
//  Created by Jindrich Skeldal on 15/11/2019.
//  Copyright © 2019 Skeldal. All rights reserved.
//


import Foundation
import UIKit

extension UIAlertController {
    static func showDecision(title: String? = nil, message: String?, presentOn presenter: UIViewController, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: handler))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: handler))
        presenter.present(alert, animated: true)
    }
    
    static func showInfo(title: String? = nil, message: String?, presentOn presenter: UIViewController, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: handler))
        presenter.present(alert, animated: true)
    }
    
    static func showError(title: String?, message: String?, presentOn presenter: UIViewController, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: handler))
        presenter.present(alert, animated: true)
    }
    
    static func showSpinner(withTitle title: String? = nil, message: String?) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let activityView = UIActivityIndicatorView(style: .gray)
        activityView.startAnimating()
        alert.view.addSubview(activityView)
        return alert
    }
}
