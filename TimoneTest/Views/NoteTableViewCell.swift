//
//  NoteTableViewCell.swift
//  TimoneTest
//
//  Created by Jindrich Skeldal on 14/11/2019.
//  Copyright © 2019 Skeldal. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell {

    @IBOutlet weak var noteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
