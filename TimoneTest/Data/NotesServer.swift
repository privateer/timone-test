//
//  NotesServer.swift
//  TimoneTest
//
//  Created by Jindrich Skeldal on 14/11/2019.
//  Copyright © 2019 Skeldal. All rights reserved.
//

import Foundation

class NotesServer {
    
    public struct Note: Codable {
        var id: Int
        var title: String
    }
    
    public static let shared: NotesServer = NotesServer()
    private let _urlString: String = "http://private-9aad-note10.apiary-mock.com/notes"
    private var _notesList: [Note] = []
    
    public var notesList: [Note] { get { return _notesList } }
    public var notesCount: Int { get { return _notesList.count } }
    
    
    private func checkError(_ error:Error?, andHttpResponse response: URLResponse?) -> Bool {
        if error != nil {
            return false
        }
        
        guard let httpResponse = response as? HTTPURLResponse,
            (200...299).contains(httpResponse.statusCode) else {
                return false
        }
        
        return true
    }
    
    
    private func decodeNote(_ data: Data?) -> Note? {
        if let data = data {
            do {
                let decoder = JSONDecoder();
                let note = try decoder.decode(Note.self, from: data)
                return note
            } catch {
                return nil
            }
        }
        return nil
    }

    
    private func prepareRequest(_ urlString: String, method urlMethod: String, JSONContent isJSON: Bool ) -> URLRequest {
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = urlMethod
        if isJSON {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        return request
    }
    
    
    public func downloadNotesList(_ completion:@escaping (_ success:Bool)->Void) {
        let url = URL(string: _urlString)!
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if !self.checkError(error, andHttpResponse: response) {
                completion(false)
                return
            }
                        
            if let data = data {
                do {
                    let decoder = JSONDecoder();
                    self._notesList = try decoder.decode([Note].self, from: data)
                } catch {
                    completion(false)
                    return
                }
                
                completion(true);
                return
            }
            
            completion(false);
        }
        
        task.resume()
    }
    
    
    public func createNote(title: String, _ completion:@escaping (_ success:Bool, _ note:Note?)->Void) {
        var request = prepareRequest(_urlString, method: "POST", JSONContent: true)
        
        let dict = ["title": title]        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: []) else {
            completion(false, nil)
            return
        }
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if !self.checkError(error, andHttpResponse: response) {
                completion(false, nil)
                return
            }
            
            let note = self.decodeNote(data)
            completion(note==nil ? false : true, note)
        }
        
        task.resume()
    }
    
    
    public func updateNote(id: Int, title: String, _ completion:@escaping (_ success:Bool, _ note: Note?)->Void) {
        var request = prepareRequest(_urlString+"/"+String(id), method: "PUT", JSONContent: true)

        let dict = ["title": title]
        guard let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: []) else {
            completion(false, nil)
            return
        }
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if !self.checkError(error, andHttpResponse: response) {
                completion(false, nil)
                return
            }
            
            let note = self.decodeNote(data)
            completion(note==nil ? false : true, note)
        }
        
        task.resume()
    }
    
    
    public func downloadNote(id: Int, _ completion:@escaping (_ success:Bool, _ note:Note?)->Void) {
        let url = URL(string: _urlString+"/"+String(id))!
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if !self.checkError(error, andHttpResponse: response) {
                completion(false, nil)
                return
            }
            
            let note = self.decodeNote(data)
            completion(note==nil ? false : true, note)
        }
        
        task.resume()
    }
    
    
    public func removeNote(id: Int, _ completion:@escaping (_ success:Bool)->Void) {
        let request = prepareRequest(_urlString+"/"+String(id), method: "DELETE", JSONContent: false)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if !self.checkError(error, andHttpResponse: response) {
                completion(false)
                return
            }
            
            completion(true);
            return
        }
        
        task.resume()
    }
}
