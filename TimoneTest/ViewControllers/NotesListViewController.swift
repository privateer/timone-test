//
//  ViewController.swift
//  TimoneTest
//
//  Created by Jindrich Skeldal on 14/11/2019.
//  Copyright © 2019 Skeldal. All rights reserved.
//

import UIKit

class NotesListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var notesTableList: UITableView!

    let _cellId = "NoteTableViewCell"

    override func viewDidLoad() {
        
        super.viewDidLoad()
        refreshNotesList()
    }
    
    
    private func refreshNotesList() {
        NotesServer.shared.downloadNotesList { success in
            DispatchQueue.main.async {
                if (success) {
                    self.notesTableList.reloadData()
                } else {
                    UIAlertController.showError(title: "List wasn't loaded", message: "Can't get list of notes. Please check internet connection.", presentOn: self)
               }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NotesServer.shared.notesCount;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = notesTableList.dequeueReusableCell(withIdentifier: _cellId, for: indexPath) as! NoteTableViewCell
        
        let note = NotesServer.shared.notesList[indexPath.row]
        cell.noteLabel.text = note.title;
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            _ = UIAlertController.showDecision(title: "Remove alert!", message: "Do you really want to remove this note?", presentOn: self, handler: { action in
                switch action.style {
                case .destructive:
                    NotesServer.shared.removeNote(id: 0) { success in
                        if (success) {
                            self.refreshNotesList()
                        } else {
                            DispatchQueue.main.async {
                                UIAlertController.showError(title: "Note wasn't deleted", message: "Can't delete note. Please check internet connection.", presentOn: self)
                            }
                        }
                    }
                default: break
                }
            })
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {        
        if segue.identifier == "EditExistingNote" {
            if let indexPath = self.notesTableList.indexPathForSelectedRow {
                let vc = segue.destination as! NoteEditViewController
                vc.noteId = NotesServer.shared.notesList[indexPath.row].id
            }
        }
    }
}
