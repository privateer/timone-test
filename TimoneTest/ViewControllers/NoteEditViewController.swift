//
//  NoteEditViewController.swift
//  TimoneTest
//
//  Created by Jindrich Skeldal on 14/11/2019.
//  Copyright © 2019 Skeldal. All rights reserved.
//

import UIKit

class NoteEditViewController: UIViewController {
    
    @IBOutlet weak var noteTextView: UITextView!
    public var noteId: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let done = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(Done))
        navigationItem.rightBarButtonItems = [done]
        
        if (noteId>=0) {
            NotesServer.shared.downloadNote(id: noteId) { success, note in
                DispatchQueue.main.async {
                    if (success) {
                        self.noteTextView.text = note!.title
                    } else {
                        UIAlertController.showError(title: "Note wasn't loaded!", message: "Can't retrieve a note. Please check internet connection.", presentOn: self)
                    }
                }
            }
        }
    }
    
    
    @objc public func Done() {
        if (noteId>=0) {
            NotesServer.shared.updateNote(id: noteId, title: noteTextView.text) { success, note in
                DispatchQueue.main.async {
                    if (success) {
                        self.noteTextView.text = note!.title
                        UIAlertController.showInfo(message: "Note was successfully updated!", presentOn: self) { action in
                            self.navigationController?.popViewController(animated: true)
                        }
                    } else {
                        UIAlertController.showError(title: "Note wasn't updated!", message: "Can't update a note. Please check internet connection.", presentOn: self)
                    }
                }
            }
            
        } else {
            NotesServer.shared.createNote(title: noteTextView.text) { success, note in
                DispatchQueue.main.async {
                    if (success) {
                        self.noteTextView.text = note!.title
                        UIAlertController.showInfo(message: "Note was successfully saved!", presentOn: self) { action in
                            self.navigationController?.popViewController(animated: true)
                        }
                    } else {
                        UIAlertController.showError(title: "Note wasn't saved!", message: "Can't save a note. Please check internet connection.", presentOn: self)
                    }
                }
            }
        }
    }
}

